
CREATE DATABASE myblog;
USE myblog;
CREATE TABLE signup(
	lastname text NOT NULL,
	firstname text NOT NULL,
	username text NOT NULL,
	password text NOT NULL,
	day int(11) NOT NULL,
	month int(11) NOT NULL,
	year int(11) NOT NULL,
	gioitinh int(11) NOT NULL,
	ma int(11) NOT NULL,
	PRIMARY KEY (ma)
	 

 )ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE TABLE post(
	id int(11) AUTO_INCREMENT,
	date timestamp NOT NULL default CURRENT_TIMESTAMP,
	title text NOT NULL,
	content text NOT NULL,
	author text NOT NULL,
	image varchar(255) NOT NULL,
	ma int(11) NOT NULL,
	PRIMARY KEY(id),
	CONSTRAINT fk_post_signup FOREIGN KEY (ma) REFERENCES signup(ma) ON DELETE RESTRICT ON UPDATE CASCADE
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

 

